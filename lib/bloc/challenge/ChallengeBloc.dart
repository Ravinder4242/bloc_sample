import 'package:bloc/bloc.dart';
import 'package:bloc_sample/bloc/challenge/ChallengeEvent.dart';
import 'package:bloc_sample/bloc/challenge/ChallengeState.dart';
import 'package:bloc_sample/data/model/AllChallenges.dart';
import 'package:bloc_sample/data/repository/ChallengeRepository.dart';
import 'package:meta/meta.dart';

class ChallengeBloc extends Bloc<ChallengeEvent, ChallengeState> {
  ChallengeRepository repository;

  ChallengeBloc({@required this.repository}) : super(null);

  @override
  ChallengeState get initialState => ChallengeInitialState();

  @override
  Stream<ChallengeState> mapEventToState(ChallengeEvent event) async* {
    if (event is FetchChallengeEvent) {
      yield ChallengeLoadingState();
      try {
        AllChallenges allChallenges = await repository.getAllChallenges();
        yield ChallengeLoadedState(allChallenges: allChallenges);
      } catch (e) {
        yield ChallengeErrorState(message: e.toString());
      }
    } else if (event is OnButtonClickEvent) {
      yield ChallengeButtonState(index: event.index);
    }
  }
}
