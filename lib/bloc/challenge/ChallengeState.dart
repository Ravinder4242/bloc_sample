import 'package:bloc_sample/data/model/AllChallenges.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ChallengeState extends Equatable {}

class ChallengeInitialState extends ChallengeState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ChallengeLoadingState extends ChallengeState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ChallengeLoadedState extends ChallengeState {

  final AllChallenges allChallenges;

  ChallengeLoadedState({@required this.allChallenges});

  @override
  List<Object> get props => [allChallenges];
}

class ChallengeErrorState extends ChallengeState {

  final String message;

  ChallengeErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}

class ChallengeButtonState extends ChallengeState {

  final int index;

  ChallengeButtonState({@required this.index});

  @override
  List<Object> get props => [index];
}

