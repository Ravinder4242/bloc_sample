import 'package:equatable/equatable.dart';

abstract class ChallengeEvent extends Equatable{}

class FetchChallengeEvent extends ChallengeEvent {
  @override
  List<Object> get props => null;
}

class OnButtonClickEvent extends ChallengeEvent {

  final int index;

  OnButtonClickEvent(this.index);

  @override
  List<Object> get props => [index];
}