import 'package:bloc_sample/data/repository/ChallengeRepository.dart';
import 'package:bloc_sample/ui/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/challenge/ChallengeBloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BBFitness',
      home: BlocProvider(
        create: (context) => ChallengeBloc(repository: ChallengeRepositoryImpl()),
        child: HomePage(),
      ),
    );
  }
}
