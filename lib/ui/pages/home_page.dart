import 'package:bloc_sample/bloc/challenge/ChallengeBloc.dart';
import 'package:bloc_sample/bloc/challenge/ChallengeEvent.dart';
import 'package:bloc_sample/bloc/challenge/ChallengeState.dart';
import 'package:bloc_sample/data/model/AllChallenges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ChallengeBloc challengeBloc;
  AllChallenges allChallenges;

  @override
  void initState() {
    super.initState();
    challengeBloc = BlocProvider.of<ChallengeBloc>(context);
    challengeBloc.add(FetchChallengeEvent());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Builder(
        builder: (context) {
          return Material(
            child: Scaffold(
              appBar: AppBar(
                title: Text("Challenges"),
                actions: <Widget>[
                  IconButton(
                    icon: Icon(Icons.refresh),
                    onPressed: () {
                      challengeBloc.add(FetchChallengeEvent());
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.info),
                    onPressed: () {
                      //navigateToAoutPage(context);
                    },
                  )
                ],
              ),
              body: Container(
                child: BlocListener<ChallengeBloc, ChallengeState>(
                  listener: (context, state) {
                    if (state is ChallengeErrorState) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(state.message),
                        ),
                      );
                    }
                  },
                  child: BlocBuilder<ChallengeBloc, ChallengeState>(
                    builder: (context, state) {
                      if (state is ChallengeInitialState) {
                        return buildLoading();
                      } else if (state is ChallengeLoadingState) {
                        return buildLoading();
                      } else if (state is ChallengeLoadedState) {
                        allChallenges = state.allChallenges;
                        return buildChallengeList(state.allChallenges, -1);
                      } else if (state is ChallengeErrorState) {
                        return buildErrorUi(state.message);
                      } else if (state is ChallengeButtonState) {
                        return buildChallengeList(allChallenges, state.index);
                      } else {
                        return Container();
                      }
                    },
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildChallengeList(AllChallenges allChallenges, int indexs) {
    return ListView.builder(
      itemCount: allChallenges.myChallengesData.length,
      itemBuilder: (ctx, pos) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            child: ListTile(
              leading: ClipRect(
                clipBehavior: Clip.antiAlias,
                child: Hero(
                  tag: allChallenges.myChallengesData[pos].thumbnail,
                  child: Image.network(
                    allChallenges.myChallengesData[pos].thumbnail,
                    fit: BoxFit.cover,
                    height: 100.0,
                    width: 100.0,
                  ),
                ),
              ),
              title: (indexs != -1 && indexs == pos) ? Text("WONDER WORLD") : Text(allChallenges.myChallengesData[pos].title),
              subtitle: Text(allChallenges.myChallengesData[pos].challengeId),
            ),
            onTap: () {
              challengeBloc.add(OnButtonClickEvent(pos));
              //navigateToArticleDetailPage(context, allChallenges.myChallengesData[pos]);
            },
          ),
        );
      },
    );
  }

// void navigateToArticleDetailPage(BuildContext context, Articles article) {
//   Navigator.push(context, MaterialPageRoute(builder: (context) {
//     return ArticleDetailPage(
//       article: article,
//     );
//   }));
// }

// void navigateToAoutPage(BuildContext context) {
//   Navigator.push(context, MaterialPageRoute(builder: (context) {
//     return AboutPage();
//   }));
// }
}
