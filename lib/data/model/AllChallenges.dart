import 'dart:convert';

import 'Challenge.dart';


AllChallenges allChallengesFromJson(String str) => AllChallenges.fromJson(json.decode(str));

String allChallengesToJson(AllChallenges data) => json.encode(data.toJson());

class AllChallenges {
  AllChallenges({
    this.statusCode,
    this.statusMessage,
    this.message,
    this.data,
    this.myChallengesData,
  });

  int statusCode;
  String statusMessage;
  String message;
  List<Datum> data;
  List<Challenge> myChallengesData;

  factory AllChallenges.fromJson(Map<String, dynamic> json) => AllChallenges(
    statusCode: json["statusCode"],
    statusMessage: json["statusMessage"],
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    myChallengesData: List<Challenge>.from(json["myChallengesData"].map((x) => Challenge.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode,
    "statusMessage": statusMessage,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "myChallengesData": List<dynamic>.from(myChallengesData.map((x) => x)),
  };
}

class Datum {
  Datum({
    this.live,
    this.upcomming,
  });

  List<Challenge> live;
  List<Challenge> upcomming;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    live: List<Challenge>.from(json["Live"].map((x) => Challenge.fromJson(x))),
    upcomming: List<Challenge>.from(json["Upcomming"].map((x) => Challenge.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "Live": List<dynamic>.from(live.map((x) => x.toJson())),
    "Upcomming": List<dynamic>.from(upcomming.map((x) => x.toJson())),
  };
}
