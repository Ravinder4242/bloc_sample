class Challenge {
  Challenge(
      {this.id,
      this.challengeId,
      this.createdDateTime,
      this.updatedDateTime,
      this.startDateTime,
      this.endDateTime,
      this.isDeleted,
      this.isPremium,
      this.title,
      this.status,
      this.thumbnail,
      this.description,
      this.videoUrl,
      this.workoutType,
      this.duration,
      this.noOfWeeks,
      this.price,
      this.isSubscribed,
      this.releaseDate,
      this.totalNoOfMovements,
      this.challengeKey,
      this.isSpecial,
      this.trainerProfileImage});

  String id;
  String challengeId;
  DateTime createdDateTime;
  DateTime updatedDateTime;
  DateTime startDateTime;
  DateTime endDateTime;
  bool isDeleted;
  bool isPremium;
  String title;
  String status;
  String thumbnail;
  String description;
  String videoUrl;
  String workoutType;
  int duration;
  int noOfWeeks;
  int price;
  bool isSubscribed;
  DateTime releaseDate;
  int totalNoOfMovements;
  String challengeKey;
  bool isSpecial;
  String trainerProfileImage;

  factory Challenge.fromJson(Map<String, dynamic> json) => Challenge(
        id: json["_id"],
        challengeId: json["challengeId"],
        createdDateTime: json["createdDateTime"] != null
            ? DateTime.parse(json["createdDateTime"])
            : new DateTime.now(),
        updatedDateTime: json["updatedDateTime"] != null
            ? DateTime.parse(json["updatedDateTime"])
            : new DateTime.now(),
        startDateTime: json["startDateTime"] != null
            ? DateTime.parse(json["startDateTime"])
            : new DateTime.now(),
        endDateTime: json["endDateTime"] != null
            ? DateTime.parse(json["endDateTime"])
            : new DateTime.now(),
        isDeleted: json["isDeleted"],
        isPremium: json["isPremium"],
        title: json["title"],
        status: json["status"],
        thumbnail: json["thumbnail"],
        description: json["description"],
        videoUrl: json["videoUrl"],
        workoutType: json["workoutType"],
        duration: json["duration"],
        noOfWeeks: json["noOfWeeks"],
        price: json["price"],
        isSubscribed: json["isSubscribed"],
        totalNoOfMovements: json["totalNoOfMovements"],
        releaseDate: json["releaseDate"] != null
            ? DateTime.parse(json["releaseDate"])
            : new DateTime.now(),
        challengeKey: json["challengeKey"],
        isSpecial: json["isSpecial"],
        trainerProfileImage: json["trainerProfileImage"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "challengeId": challengeId,
        "createdDateTime": createdDateTime.toIso8601String(),
        "updatedDateTime": updatedDateTime.toIso8601String(),
        "startDateTime": startDateTime.toIso8601String(),
        "endDateTime": endDateTime.toIso8601String(),
        "isDeleted": isDeleted,
        "isPremium": isPremium,
        "title": title,
        "status": status,
        "thumbnail": thumbnail,
        "description": description,
        "videoUrl": videoUrl,
        "workoutType": workoutType,
        "duration": duration,
        "noOfWeeks": noOfWeeks,
        "price": price,
        "isSubscribed": isSubscribed,
        "totalNoOfMovements": totalNoOfMovements,
        "releaseDate": releaseDate.toIso8601String(),
        "challengeKey": challengeKey,
        "isSpecial": isSpecial,
        "trainerProfileImage": trainerProfileImage,
      };
}
