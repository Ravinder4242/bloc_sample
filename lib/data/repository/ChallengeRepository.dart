import 'dart:convert';

import 'package:bloc_sample/data/model/AllChallenges.dart';
import 'package:http/http.dart' as http;

abstract class ChallengeRepository {
  Future<AllChallenges> getAllChallenges();
}

class ChallengeRepositoryImpl implements ChallengeRepository {

  @override
  Future<AllChallenges> getAllChallenges() async {
    var response = await http.get(
        Uri.parse(
            "https://qa-api-bbfitness.fanworld.io/challenges/allChallenges?userId=60d5e9a483834f7a225b54fd&limit=10&offset=0"),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json; charset=utf-8",
          "auth":
              "4e866e8787f19e30c623e607db364118354c305dca8c66caf7418d115a307579"
        });
    if (response.statusCode == 200 || response.statusCode == 201) {
      var data = json.decode(response.body);
      AllChallenges allChallenges = allChallengesFromJson(json.encode(data));
      return allChallenges;
    } else {
      throw Exception();
    }
  }

}
